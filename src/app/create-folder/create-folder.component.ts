import { Component, OnInit, Input } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Folder, File } from './../file-tree/file-tree.model';
import { FileTreeState } from './../file-tree/file-tree.state';
import { AddFolder, DeleteFolder} from '../file-tree/file-tree.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-folder',
  templateUrl: './create-folder.component.html',
  styleUrls: ['./create-folder.component.css']
})
export class CreateFolderComponent implements OnInit {

  constructor(private store: Store) { }

  @Input() parent = null;

  deleteFolder(path) {
    this.store.dispatch(new DeleteFolder(path));
  }

  addFolder(path, parent) {

    this.deleteFolder('');

    console.log(parent);

    if (!parent) {
      parent = '/';
    } else {
      parent = parent.path;
    }

    console.log(parent);

    const file: File[] = [];
    const folder: Folder[] = [];
    this.store.dispatch(new AddFolder({path: '', name: path, files: file, show: false, folders: folder, parent: parent}));
  }

  focusOnInput(event) {
    event.stopPropagation();
  }

  ngOnInit() {
  }
}

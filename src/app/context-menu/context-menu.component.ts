import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Store } from '@ngxs/store';
import { Folder, File } from './../file-tree/file-tree.model';
import { AddFolder, AddFile, ToggleContextMenu, ToggleFolder } from '../file-tree/file-tree.actions';

@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.css'],
})
export class ContextMenuComponent implements OnInit {
  @Input() x = 0;
  @Input() y = 0;
  @Input() context = null;
  @Input() item = null;

  contextMenu: boolean;

  // @ViewChild('contextMenuContainer') public contextMenu2 : boolean;

  constructor(private store: Store) { }

  toggleFolder(folder, show) {
    this.store.dispatch(new ToggleFolder(folder.path, show));
  }

  toggleContextMenu(state) {
    this.store.dispatch(new ToggleContextMenu(state));
  }

  ngOnInit() {
  }

  newFolder(item) {
    this.toggleContextMenu(false);

    if (item === 'root') {
      item = '/';
    } else {
      this.toggleFolder(item, 'true');
      item = item.path;
    }

    const file: File[] = [];
    const folder: Folder[] = [];

    this.store.dispatch(new AddFolder({path: '', name: '', files: file, show: false, folders: folder, parent: item}));
  }

  newFile(item) {
    this.toggleContextMenu(false);

    if (item === 'root') {
      item = '/';
    } else {
      this.toggleFolder(item, 'true');
      item = item.path;
    }

    this.store.dispatch(new AddFile({path: '', name: '', parent: item}));
  }
}

import { TestBed, async } from '@angular/core/testing';
// Main modules
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
  MatIconRegistry,
  MatIconModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatCheckboxModule
} from '@angular/material';
// Custom libraries.
import { MyLibModule } from 'my-lib';
// Main components.
import { AppComponent } from './app.component';
// Page routing modules.
import { SidenavRoutingModule } from './sidenav/sidenav-routing.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SidenavService } from './sidenav/sidenav.service';
// Page routing components.
import { AboutComponent } from './about/about.component';
import { BlogComponent } from './blog/blog.component';
import { ProjectsComponent } from './projects/projects.component';
import { ResumeComponent } from './resume/resume.component';
import { TestingComponent } from './testing/testing.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
// State management components
import { CreateFolderComponent } from './create-folder/create-folder.component';
import { ReadTreeComponent } from './read-tree/read-tree.component';
import { CreateFileComponent } from './create-file/create-file.component';
import { ReadFileComponent } from './read-file/read-file.component';
// Stete components
import { FileTreeState } from './file-tree/file-tree.state';
import { ContextMenuComponent } from './context-menu/context-menu.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        FormsModule,
        BrowserAnimationsModule,
        MatIconModule,
        HttpClientModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatCheckboxModule,
        SidenavRoutingModule,
        MyLibModule,
        NgxsModule.forRoot([
        FileTreeState,
        ]),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsLoggerPluginModule.forRoot(),
        PdfViewerModule,
      ],
      declarations: [
        AppComponent,
        SidenavComponent,
        AboutComponent,
        BlogComponent,
        ProjectsComponent,
        ResumeComponent,
        TestingComponent,
        PageNotFoundComponent,
        CreateFolderComponent,
        ReadTreeComponent,
        CreateFileComponent,
        ReadFileComponent,
        ContextMenuComponent,
      ],
      providers: [
        SidenavService,
        {provide: APP_BASE_HREF, useValue: '/'},
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
//   it(`should have as title 'app'`, async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     const app = fixture.debugElement.componentInstance;
//     expect(app.title).toEqual('app');
//   }));
//   it('should render title in a h1 tag', async(() => {
//     const fixture = TestBed.createComponent(AppComponent);
//     fixture.detectChanges();
//     const compiled = fixture.debugElement.nativeElement;
//     expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
//   }));
});

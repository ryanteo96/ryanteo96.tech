import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Folder, File} from './../file-tree/file-tree.model';
import { FileTreeState } from './../file-tree/file-tree.state';
import { ToggleFolder, ToggleContextMenu, DeleteFolder, DeleteFile } from '../file-tree/file-tree.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-read-tree',
  templateUrl: './read-tree.component.html',
  styleUrls: ['./read-tree.component.css'],
})
export class ReadTreeComponent implements OnInit {

  @Select(FileTreeState.getFolders) folders$: Observable<Folder>;

  @Select(FileTreeState.getFiles) files$: Observable<File>;

  @Select(FileTreeState.getContextMenuState) contextMenu$: Observable<boolean>;

  constructor(private store: Store) {
  }

  contextmenuX = 0;
  contextmenuY = 0;
  context = null;
  item = null;

  ngOnInit() {
  }

  toggleFolder(folder, show) {
    this.store.dispatch(new ToggleFolder(folder.path, show));
  }

  toggleContextMenu(state) {
    this.store.dispatch(new ToggleContextMenu(state));
  }

  deleteFolder(path) {
    this.store.dispatch(new DeleteFolder(path));
  }

  deleteFile(path) {
    this.store.dispatch(new DeleteFile(path));
  }

  onrightclick(event, item, context) {
    event.stopPropagation();
    const div = document.getElementById('fileTree');
    this.contextmenuX = event.clientX - div.offsetLeft;
    this.contextmenuY = event.clientY - div.offsetTop;
    this.context = context;
    this.item = item;
    this.deleteFolder('');
    this.deleteFile('');
    this.toggleContextMenu(true);
  }

  disablecontextmenu() {
    this.deleteFolder('');
    this.deleteFile('');
    this.toggleContextMenu(false);
  }

  oninputclick(event) {
    event.stopPropagation();
  }
}

import { State, Action, StateContext, Selector } from '@ngxs/store';
import {
    Folder,
    File,
} from './../file-tree/file-tree.model';
import {
    AddFolder,
    AddFile,
    ToggleFolder,
    ToggleContextMenu,
    DeleteFolder,
    DeleteFile,
} from './../file-tree/file-tree.actions';

export interface FileTreeStateModel {
    folders: Folder[];
    files: File[];
    contextMenu: boolean;
}

@State<FileTreeStateModel>({
    name: 'fileTree',
    defaults: {
        folders: [],
        files: [],
        contextMenu: false,
    },
})

export class FileTreeState {
    @Selector()
    static getFolders(state: FileTreeStateModel) {
        return state.folders;
    }

    @Selector()
    static getFiles(state: FileTreeStateModel) {
        return state.files;
    }

    @Selector()
    static getContextMenuState(state: FileTreeStateModel): boolean {
        return state.contextMenu;
    }

    @Action(AddFolder)
    addFolder({ getState, patchState }: StateContext<FileTreeStateModel>, { folder }: AddFolder) {
        const state = getState();

        if (!folder.name) {
            if (folder.parent !== '/') {
                const parent = state.folders.filter(a => a.path === folder.parent)[0].folders;
                const folders = parent.slice();
                folders.splice(parent.length, 0, folder);
                state.folders.find(a => a.path === folder.parent).folders = folders;

                patchState({
                    folders: [...state.folders, folder],
                });
            }

            if (folder.parent === '/') {
                patchState({
                    folders: [...state.folders, folder],
                });
            }
        } else {
            if (folder.parent !== '/') {
                const parent = state.folders.filter(a => a.path === folder.parent)[0].folders;

                if (parent.every(a => a.name !== folder.name)) {
                    const folders = parent.slice();
                    folders.splice(parent.length, 0, folder);
                    state.folders.find(a => a.path === folder.parent).folders = folders;
                    folder.path = folder.parent + '/' + folder.name;

                    patchState({
                        folders: [...state.folders, folder],
                    });
                }
            }

            if (folder.parent === '/') {
                const temp = state.folders.filter(a => a.parent === folder.parent);

                if (temp.every(a => a.name !== folder.name)) {
                    folder.path = folder.parent + folder.name;

                    patchState({
                        folders: [...state.folders, folder],
                    });
                }
            }
        }
    }

    @Action(AddFile)
    addFile({ getState, patchState }: StateContext<FileTreeStateModel>, { file }: AddFile) {
        const state = getState();

        if (!file.name) {
            if (file.parent !== '/') {
                const parent = state.folders.filter(a => a.path === file.parent)[0].files;
                const files = parent.slice();
                files.splice(parent.length, 0, file);
                state.folders.find(a => a.path === file.parent).files = files;

                patchState({
                    files: [...state.files, file],
                });
            }

            if (file.parent === '/') {
                patchState({
                    files: [...state.files, file],
                });
            }
        } else {
            if (file.parent !== '/') {
                const parent = state.folders.filter(a => a.path === file.parent)[0].files;

                if (parent.every(a => a.name !== file.name)) {
                    const files = parent.slice();
                    files.splice(parent.length, 0, file);
                    state.folders.find(a => a.path === file.parent).files = files;
                    file.path = file.parent + '/' + file.name;

                    patchState({
                        files: [...state.files, file],
                    });
                }
            }

            if (file.parent === '/') {
                const temp = state.folders.filter(a => a.parent === file.parent);

                if (temp.every(a => a.name !== file.name)) {
                    file.path = file.parent + file.name;

                    patchState({
                        files: [...state.files, file],
                    });
                }
            }
        }
    }

    @Action(ToggleFolder)
    toggleFolder({ getState }: StateContext<FileTreeStateModel>, { path, show }: ToggleFolder) {
        const state = getState();

        if (!show) {
            const currentShow = state.folders.find(a => a.path === path).show;

            if (currentShow) {
                state.folders.find(a => a.path === path).show = false;
            } else {
                state.folders.find(a => a.path === path).show = true;
            }
        } else {
            if (show === 'true') {
                state.folders.find(a => a.path === path).show = true;
            }

            if (show === 'false') {
                state.folders.find(a => a.path === path).show = false;
            }
        }
    }

    @Action(ToggleContextMenu)
    toggleContextMenu({ getState, patchState }: StateContext<FileTreeStateModel>, { payload }: ToggleContextMenu) {
        const state = getState();

        patchState({
            ...state,
            contextMenu: payload,
        });
    }

    @Action(DeleteFolder)
    deleteFolder({ getState, patchState }: StateContext<FileTreeStateModel>, { path }: DeleteFolder) {
        const state = getState();
        const folder = state.folders.filter(a => a.path === path)[0];

        if (folder && folder.parent !== '/') {
            const folders = state.folders.find(a => a.path === folder.parent).folders;
            state.folders.find(a => a.path === folder.parent).folders = folders.filter(a => a.path !== path);
        }

        patchState({
            folders: state.folders.filter(a => a.path !== path),
        });
    }

    @Action(DeleteFile)
    deleteFile({ getState, patchState }: StateContext<FileTreeStateModel>, { path }: DeleteFile) {
        const state = getState();
        const file = state.files.filter(a => a.path === path)[0];

        if (file && file.parent !== '/') {
            const files = state.folders.find(a => a.path === file.parent).files;
            state.folders.find(a => a.path === file.parent).files = files.filter(a => a.path !== path);
        }

        patchState({
            files: state.files.filter(a => a.path !== path),
        });
    }
}

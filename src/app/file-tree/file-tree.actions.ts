import { Folder, File } from './../file-tree/file-tree.model';

export class AddFolder {
    static readonly type = '[Folder] Add';

    constructor(public folder: Folder) {}
}

export class AddFile {
    static readonly type = '[File] Add';

    constructor(public file: File) {}
}

export class ToggleFolder {
    static readonly type = '[Folder] Toggle';

    constructor(public path: string, public show: string) {}
}

export class ToggleContextMenu {
    static readonly type = '[ContextMenu] Toggle';

    constructor(public payload: boolean) {}
}

export class DeleteFolder {
    static readonly type = '[Folder] Delete';

    constructor(public path: string) {}
}

export class DeleteFile {
    static readonly type = '[File] Delete';

    constructor(public path: string) {}
}

export interface Folder {
    path: string;
    name: string;
    parent: string;
    show: boolean;
    folders: Folder[];
    files: File[];
}

export interface File {
    path: string;
    name: string;
    parent: string;
}

import { Component, OnInit, Input } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Folder, File } from './../file-tree/file-tree.model';
import { FileTreeState } from './../file-tree/file-tree.state';
import { AddFile, DeleteFile} from '../file-tree/file-tree.actions';

@Component({
  selector: 'app-create-file',
  templateUrl: './create-file.component.html',
  styleUrls: ['./create-file.component.css'],
})
export class CreateFileComponent implements OnInit {

  constructor(private store: Store) {}

  @Input() parent = null;

  deleteFile(path) {
    this.store.dispatch(new DeleteFile(path));
  }

  addFile(path, parent) {

    this.deleteFile('');

    console.log(parent);

    if (!parent) {
      parent = '/';
    } else {
      parent = parent.path;
    }

    this.store.dispatch(new AddFile({path: '', name: path, parent: parent}));
  }

  focusOnInput(event) {
    event.stopPropagation();
  }

  ngOnInit() {
  }
}

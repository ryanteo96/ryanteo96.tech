import { Component, ViewChild } from '@angular/core';
import { SidenavService } from './sidenav/sidenav.service';
import { SidenavComponent } from './sidenav/sidenav.component';
// import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(private sidenav: SidenavService) {}

  toggleSidenav() {
    this.sidenav.toggle();
  }
}

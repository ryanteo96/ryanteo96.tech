export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCb8yCvIG2rhKZhx0BVb38NICiMVuV4EdU',
    authDomain: 'personal-website-d625f.firebaseapp.com',
    databaseURL: 'https://personal-website-d625f.firebaseio.com',
    projectId: 'personal-website-d625f',
    storageBucket: 'personal-website-d625f.appspot.com',
    messagingSenderId: '2733861501'
  }
};
